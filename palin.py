#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Public domain

import itertools

def palin (d):
  '''Nombre palindrome (avec un nombre pair de chiffres) défini à partir d'un tableau de chiffres
  @param d chiffres
  @return nombre'''
  n = 0
  for dd in itertools.chain (d, reversed (d)):
    n *= 10
    n += dd
  return n

def ispalin (sn):
  '''Teste si un nombre est un palindrome
  @param sn chaîne représentant un nombre
  @return résultat du test'''
  for i in range (len (sn) / 2):
    if sn[i] != sn[-(i+1)]: return False
  return True

def test_solution (n1, n2):
  '''Teste si une solution convient. On suppose qu'on sait déjà
  que les deux nombres sont des palindromes de 6 chiffres composés de 3 chiffres distincts et n'ont pas de chiffre en commun
  @param n1 premier nombre
  @param n2 deuxième nombre
  @return résultat du test'''
  s = n1 + n2
  d = abs (n1 - n2)
  sn1, sn2, ss, sd = str (n1), str (n2), str (s), str (d)
  if not (ispalin (ss) and ispalin (sd)): return False
  if len (ss) != 7: return False
  for sn in sn1, sn2:
    if any ([dss in sn for dss in ss]): return False
  for i in range (1, len (sd) / 2):
    if sd[i] < sd[i-1]: return False
  return True


# parcourir les ensembles de 6 chiffres parmi 10 pour définir les 2 nombres, et tester la solution
for d in itertools.permutations (range (10), 6):
  n1, n2 = palin (d[0:3]), palin (d[3:6])
  if test_solution (n1, n2):
    print n1, n2
