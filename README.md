This project is to share programs to answer the [enigma](http://enigme.blog.lemonde.fr/2015/11/30/nombres-palindromes) on Florian Gouthière's blog on [Le Monde](http://www.lemonde.fr/).

Ce projet a pour objet de partager des programmes pour résoudre l'[énigme](http://enigme.blog.lemonde.fr/2015/11/30/nombres-palindromes) de Florian Gouthière sur le site [du Monde](http://www.lemonde.fr/).
